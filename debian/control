Source: node-katex
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Section: javascript
Testsuite: autopkgtest-pkg-nodejs
Priority: optional
Build-Depends: debhelper-compat (= 13)
 , dh-sequence-nodejs
 , babel-minify (>= 0.5.0-4~)
 , cleancss
 , help2man
 , jest
 , node-babel-loader (>= 8.0~)
 , node-babel7
 , node-browserslist (>= 4.12~)
 , node-css-loader (>= 2.1~)
 , node-express
 , node-file-loader (>= 3.0.1~)
 , node-htmlparser2 <!nocheck>
 , node-js-yaml
 , node-less (>= 3.0~)
 , node-less-loader
 , node-mini-css-extract-plugin
 , node-nomnom
 , node-object-assign
 , node-pako
 , node-postcss-loader
 , node-postcss-preset-evergreen
 , node-require-from-string
 , node-read-pkg-up
 , node-rollup-plugin-alias
 , node-rollup-plugin-babel (>= 5.2~)
 , node-type-fest
 , node-typescript
 , rollup (>= 2.34~)
 , uglifyjs
 , webpack (>= 4.43~)
 , zip
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/js-team/node-katex
Vcs-Git: https://salsa.debian.org/js-team/node-katex.git
Homepage: https://github.com/Khan/KaTeX#readme
Rules-Requires-Root: no

Package: katex
Architecture: all
Depends: ${misc:Depends}
 , libjs-katex (=${binary:Version})
 , node-commander
 , nodejs:any
Provides: ${nodejs:Provides}
 , node-katex (= ${binary:Version})
Description: Fast math typesetting for the web (command line interface)
 KaTeX is a fast, easy-to-use JavaScript library for TeX math rendering on the
 web.
 .
 KaTeX supports all major browsers, including Chrome, Safari, Firefox, Opera,
 Edge, and IE 9 - IE 11.
 .
 This package provides a command line interface for KaTeX, which can be used to
 render TeX to HTML. By default, CLI will take the input from standard input.
 .
 Node.js is an event-based server-side JavaScript engine.

Package: libjs-katex
Architecture: all
Depends: ${misc:Depends}
 , fonts-katex (=${binary:Version})
Recommends: javascript-common
Multi-Arch: foreign
Description: Fast math typesetting for the web (for browsers)
 KaTeX is a fast, easy-to-use JavaScript library for TeX math rendering on the
 web.
 .
 KaTeX supports all major browsers, including Chrome, Safari, Firefox, Opera,
 Edge, and IE 9 - IE 11.
 .
 This package provides files targeted at browsers.

Package: fonts-katex
Architecture: all
Section: fonts
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Fast math typesetting for the web (fonts)
 KaTeX is a fast, easy-to-use JavaScript library for TeX math rendering on the
 web.
 .
 KaTeX supports all major browsers, including Chrome, Safari, Firefox, Opera,
 Edge, and IE 9 - IE 11.
 .
 This package provides fonts from KaTeX project.
